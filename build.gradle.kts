import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    kotlin("jvm") version "1.4.0"
    application
}
group = "maxdistructo.svrmgmt"
version = "1.0"

repositories {
    mavenCentral()
}
dependencies {
    testImplementation(kotlin("test-junit"))
    implementation("io.netty", "netty-all", "4.1.51.Final")
    implementation("org.json", "json", "20200518")
    implementation("commons-io", "commons-io", "2.7")
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:1.3.9")
    implementation("com.github.oshi", "oshi-core", "5.2.4")
    implementation("commons-codec", "commons-codec", "1.14")
}
tasks.withType<KotlinCompile>() {
    kotlinOptions.jvmTarget = "1.8"
}
application {
    mainClassName = "MainKt"
}