package maxdistructo.svrmgmt

import org.apache.commons.io.FileUtils
import org.json.JSONArray
import org.json.JSONObject
import org.json.JSONTokener
import java.io.File
import java.io.FileWriter
import java.io.IOException
import java.nio.charset.Charset
import java.nio.file.Paths
import java.nio.file.Files



object Utils {
    private val currentRelativePath = Paths.get("")
    val s = currentRelativePath.toAbsolutePath().toString()

    /**
     *   Creates a new String using the provided Array and a position to start at. This start position is based on the number in the array.
     *   Ex. To cut out the first value in an array and make it into a string, input 1 for startAt
     *   @param input The array to draw from
     *   @param startAt The value to start from
     *   @return A string built off of all the values the array after the specified value.
     */
    fun makeNewString(input: List<Any>, startAt: Int): String {
        val stringBuilder = StringBuilder()
        var i = startAt
        while (i < input.size) {
            if (i - 1 == input.size) {
                stringBuilder.append(input[i])
            } else {
                stringBuilder.append(input[i])
                stringBuilder.append(" ")
            }
            i++
        }
        return stringBuilder.toString()
    }

    /**
     *   Converts the input to a Long value. Returns null if the convert fails.
     *   @param o The object to try and convert to Long
     *   @return The long value of the input or null if it is unable to be converted
     */

    fun convertToLong(o: Any): Long? {
        return try {
            java.lang.Long.valueOf(o.toString())
        } catch (e: Exception) {
            null
        }
    }

    /**
     *   Converts the input to an Int value. Returns null if the convert fails or is impossible.
     *   @param in The object to convert
     *   @return The converted object or null
     */

    fun convertToInt(`in`: Any): Int {
        return Integer.valueOf(`in`.toString())
    }

    /**
     *   Converts the inputed {@link org.json.JSONArray} to an Array of Strings
     *   @param array The JSON array to convert to String
     *   @return The string array or null if the convert is impossible or fails
     */

    fun toStringArray(array: JSONArray?): Array<String?>? {
        if (array == null)
            return null

        val arr = arrayOfNulls<String>(array.length())
        for (i in arr.indices) {
            arr[i] = array.optString(i)
        }
        return arr
    }

    /**
     *   Similar to #makeNewString but puts new lines between the values instead of spaces
     *   @param input The array to convert
     *   @param startAt The spot in the array to start reading at
     *   @return The String with new lines
     */

    fun makeNewLineString(input: List<String?>, startAt: Int): String {
        val stringBuilder = StringBuilder()
        var i = startAt
        while (i < input.size) {
            stringBuilder.append(input[i])
            stringBuilder.append("\n")
            i++
        }
        return stringBuilder.toString()
    }

    /**
     *   Reads the JSON from the specified file
     *   @param fileName The path to the file in relation to the location of the running directory
     *   @return The JSON object from the file.
     */

    fun readJSONFromFile(fileName: String): JSONObject {
        println("[DEBUG] read JSON from file")
        val file = File(s + fileName)
        if(!file.exists()){
            createNewJSONFile(fileName)
        }
        val uri = file.toURI()
        var tokener: JSONTokener? = null
        try {
            tokener = JSONTokener(uri.toURL().openStream())
        } catch (e: IOException) {
            e.printStackTrace()
        }

        return if (tokener != null) {
            JSONObject(tokener)
        } else {
            throw NullPointerException()
        }
    }

    /**
     * Writes the provided JSONObject to file
     * @param path The relative location to the running directory
     * @param jsonObject The object to write to File
     */

    fun writeJSONToFile(path: String, jsonObject: JSONObject) {
        val file = File(s + path)
        try {
            file.createNewFile()
        } catch (e: IOException) {}

        try {
            FileWriter(s + path).use { fileWriter ->
                fileWriter.write(jsonObject.toString())
            }
        } catch (e: IOException) {
            e.printStackTrace()
        }

    }

    /**
     * Creates a new file with an empty JSONObject in it
     * @param string The relative location to the running directory
     */
    fun createNewJSONFile(string : String){
        val file = File(s + string)
        file.createNewFile()
        writeJSONToFile(string, JSONObject())
    }

    /**
     * Reads all lines of a file and returns it as a List<String>
     * @param file The relative location of the file to the running directory
     */

    fun readAllLines(file : String) : List<String>{
        return readAllLines(file, true)
    }

    /**
     * Reads all lines of a file and returns it as a List<String>
     * @param file The file
     * @param isRelative Should the file look for the file in a relative method or not
     */

    fun readAllLines(file : String, isRelative : Boolean) : List<String>{
        return if(isRelative){
            FileUtils.readLines(File(s + file), "utf-8") as List<String>
        }
        else{
            FileUtils.readLines(File(file), "utf-8") as List<String>
        }
    }

    /**
     * Array<Byte> to String converter
     * @param array The Byte array to convert
     * @return A string of the byte array
     */

    fun byteArrayToString(array : ByteArray) : String{
        val sb = StringBuilder()
        for(value in array){
            sb.append(Integer.toString((value + 0xff) + 0x100, 16).substring(1))
        }
        return sb.toString()
    }



}
