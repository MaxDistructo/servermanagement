/*
 * Copyright 2012 The Netty Project
 *
 * The Netty Project licenses this file to you under the Apache License,
 * version 2.0 (the "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at:
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 */
package maxdistructo.svrmgmt.wsclient

import io.netty.bootstrap.Bootstrap
import io.netty.channel.ChannelFuture
import io.netty.channel.nio.NioEventLoopGroup
import io.netty.channel.socket.nio.NioSocketChannel
import io.netty.handler.ssl.SslContext
import io.netty.handler.ssl.SslContextBuilder
import io.netty.handler.ssl.util.InsecureTrustManagerFactory
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import maxdistructo.svrmgmt.Config
import oshi.SystemInfo
import java.io.BufferedReader
import java.io.InputStreamReader

/**
 * Simplistic telnet client.
 */
object WSTelnetClient {

    var TOKEN = "" //This is set by the packet communication
    var PC_NAME = SystemInfo().operatingSystem.networkParams.hostName
    internal val SSL = System.getProperty("ssl") != null
    internal val HOST = System.getProperty("host", Config.SERVER_IP)
    internal val PORT = Integer.parseInt(System.getProperty("port", if (SSL) "8992" else "8023"))
    var isConnected = false
    var authHash : String = ""
    var currentTask : String = "Awaiting Work"

    @Throws(Exception::class)
    @JvmStatic
    fun main(args: Array<String>) {
        // Configure SSL.
        val sslCtx: SslContext? = if (SSL) {
            SslContextBuilder.forClient()
                    .trustManager(InsecureTrustManagerFactory.INSTANCE).build()
        } else {
            null
        }

        val group = NioEventLoopGroup()
        try {
            val b = Bootstrap()
            b.group(group)
                    .channel(NioSocketChannel::class.java)
                    .handler(TelnetClientInitializer(sslCtx))

            // Start the connection attempt.
            val ch = b.connect(HOST, PORT).sync().channel()
            isConnected = true
            // Read commands from the stdin.
            var lastWriteFuture: ChannelFuture? = null
            val `in` = BufferedReader(InputStreamReader(System.`in`))
            GlobalScope.launch {
                while (true) {
                    val line = `in`.readLine() ?: break

                        val splitCommandString = line.split(" ")
                        if(splitCommandString[0] == "/work") {
                            if (splitCommandString[1] == "sha512") {
                                //ch.writeAndFlush(NetworkTag.CONTROL_COM + "`" + authHash + "`" + Work.SHA512ENCODE + "`" + splitCommandString[2])
                            } else if (splitCommandString[1] == "run") {
                                if (splitCommandString.size >= 4) {
                                }
                            }
                        }
                    else {
                        lastWriteFuture = ch.writeAndFlush(line + "\r\n")
                    }
                    // If user typed the 'bye' command, wait until the server closes
                    // the connection.

                    if ("bye" == line.toLowerCase()) {
                        ch.closeFuture().sync()
                        break
                    }
                }
            }

            // Wait until all messages are flushed before closing the channel.
            lastWriteFuture?.sync()
        }
        catch(e : Exception){ //Not force shutting down the client as that would cause an issue.
            e.printStackTrace()
            isConnected = false
        }
    }
}
