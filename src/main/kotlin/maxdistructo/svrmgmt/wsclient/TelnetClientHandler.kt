/*
 * Copyright 2012 The Netty Project
 *
 * The Netty Project licenses this file to you under the Apache License,
 * version 2.0 (the "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at:
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 */
package maxdistructo.svrmgmt.wsclient

import io.netty.channel.ChannelHandler.Sharable
import io.netty.channel.ChannelHandlerContext
import io.netty.channel.SimpleChannelInboundHandler
import io.netty.util.ReferenceCountUtil
import maxdistructo.svrmgmt.enums.CommunicationMessage
import maxdistructo.svrmgmt.enums.Side
import maxdistructo.svrmgmt.packets.*
import java.net.InetAddress

/**
 * Handles a client-side channel.
 */
@Sharable
class TelnetClientHandler : SimpleChannelInboundHandler<String>() {



    override fun channelRead(ctx: ChannelHandlerContext, msg: Any) {
        
        println( "Received Message: $msg")
        try {
            val splitMessage = msg.toString().split("`")
            val side = Side.CLIENT
            when {
                splitMessage[0] == CommunicationMessage.LOGIN_SUCCESS.key -> {
                    PacketLoginSuccess.respond(splitMessage, ctx, side)
                }
                splitMessage[0] == CommunicationMessage.LOGIN_FAIL.key -> {
                    PacketLoginFailure.respond(splitMessage, ctx, side)
                }
                splitMessage[0] == CommunicationMessage.LOGIN.key -> {
                    PacketLogin.respond(splitMessage, ctx, side)
                }
                splitMessage[0] == CommunicationMessage.ACKNOWLEDGE.key -> {
                    PacketAcknowledge.respond(splitMessage, ctx, side)
                }
                splitMessage[0] == CommunicationMessage.ADD_PC.key -> {
                    PacketAddPC.respond(splitMessage, ctx, side)
                }
                splitMessage[0] == CommunicationMessage.ONLINE_PC.key -> {
                    PacketOnlinePC.respond(splitMessage, ctx, Side.WS_CLIENT)
                }
                splitMessage[0] == CommunicationMessage.DELETE_PC.key -> {
                    PacketDeletePC.respond(splitMessage, ctx, side)
                }
                splitMessage[0] == CommunicationMessage.PC_UPDATE.key -> {
                    PacketPCUpdate.respond(splitMessage, ctx, Side.WS_CLIENT)
                }
                splitMessage[0] == CommunicationMessage.MEMORY_UPDATE.key -> {
                    PacketPCMemory.respond(splitMessage, ctx, Side.WS_CLIENT)
                }
                splitMessage[0] == CommunicationMessage.STORAGE_UPDATE.key -> {
                    PacketPCStorage.respond(splitMessage, ctx, Side.WS_CLIENT)
                }
                else -> ReferenceCountUtil.release(msg)
            }
        }
        catch (e : Exception){
            e.printStackTrace()
        }
    }

    @Throws(Exception::class)
    override fun channelRead0(ctx: ChannelHandlerContext, msg: String) {} //Required to override anything else.
}
