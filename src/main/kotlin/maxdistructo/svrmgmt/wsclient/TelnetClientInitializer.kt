/*
 * Copyright 2012 The Netty Project
 *
 * The Netty Project licenses this file to you under the Apache License,
 * version 2.0 (the "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at:
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 */
package maxdistructo.svrmgmt.wsclient

import io.netty.channel.ChannelInitializer
import io.netty.channel.ChannelPipeline
import io.netty.channel.socket.SocketChannel
import io.netty.handler.codec.DelimiterBasedFrameDecoder
import io.netty.handler.codec.Delimiters
import io.netty.handler.codec.string.StringDecoder
import io.netty.handler.codec.string.StringEncoder
import io.netty.handler.ssl.SslContext

/**
 * Creates a newly configured [ChannelPipeline] for a new channel.
 */
class TelnetClientInitializer(private val sslCtx: SslContext?) : ChannelInitializer<SocketChannel>() {

    public override fun initChannel(ch: SocketChannel) {
        val pipeline = ch.pipeline()

        if (sslCtx != null) {
            pipeline.addLast(sslCtx.newHandler(ch.alloc(), WSTelnetClient.HOST, WSTelnetClient.PORT))
        }

        // Add the text line codec combination first,
        pipeline.addLast(DelimiterBasedFrameDecoder(8192, *Delimiters.lineDelimiter()))
        pipeline.addLast(DECODER)
        pipeline.addLast(ENCODER)

        // and then business logic.
        pipeline.addLast(CLIENT_HANDLER)
    }

    companion object {

        private val DECODER = StringDecoder()
        private val ENCODER = StringEncoder()

        private val CLIENT_HANDLER = TelnetClientHandler()
    }
}
