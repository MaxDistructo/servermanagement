package maxdistructo.svrmgmt.server

import java.time.Instant

class ShutdownHook : Thread() {
    override fun run(){
        println("Ending Thread")
    }
}