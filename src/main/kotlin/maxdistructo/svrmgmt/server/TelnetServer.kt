/*
 * Copyright 2012 The Netty Project
 *
 * The Netty Project licenses this file to you under the Apache License,
 * version 2.0 (the "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at:
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 */
package maxdistructo.svrmgmt.server

import io.netty.bootstrap.ServerBootstrap
import io.netty.channel.nio.NioEventLoopGroup
import io.netty.channel.socket.nio.NioServerSocketChannel
import io.netty.handler.logging.LogLevel
import io.netty.handler.logging.LoggingHandler
import io.netty.handler.ssl.SslContext
import io.netty.handler.ssl.SslContextBuilder
import io.netty.handler.ssl.util.SelfSignedCertificate
import javafx.collections.FXCollections
import javafx.collections.ObservableList
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

/**
 * Simplistic telnet server.
 */
object TelnetServer {

    internal val SSL = System.getProperty("ssl") != null
    internal val PORT = Integer.parseInt(System.getProperty("port", if (SSL) "8992" else "8023"))
    var currentStatus : String = ""
    var clientList : ObservableList<String> = FXCollections.observableArrayList()

    @Throws(Exception::class)
    @JvmStatic
    fun main(args: Array<String>) {
        Database.addUser("root", "root") //REMOVE/Change ME BEFORE DEPLOYMENT
        Runtime.getRuntime().addShutdownHook(ShutdownHook())
        // Configure SSL
        val sslCtx: SslContext?
        sslCtx = if (SSL) {
            val ssc = SelfSignedCertificate()
            SslContextBuilder.forServer(ssc.certificate(), ssc.privateKey()).build()
        } else {
            null
        }

        val bossGroup = NioEventLoopGroup(1)
        val workerGroup = NioEventLoopGroup()
        try {
            val b = ServerBootstrap()
            b.group(bossGroup, workerGroup)
                    .channel(NioServerSocketChannel::class.java)
                    .handler(LoggingHandler(LogLevel.INFO))
                    .childHandler(TelnetServerInitializer(sslCtx))

            b.bind(PORT).sync().channel().closeFuture().sync()
            GlobalScope.launch{
                while(true){
                    PCUpdater.run()
                }
            }

        } finally {
            bossGroup.shutdownGracefully()
            workerGroup.shutdownGracefully()
        }
    }
}
