/*
 * Copyright 2012 The Netty Project
 *
 * The Netty Project licenses this file to you under the Apache License,
 * version 2.0 (the "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at:
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 */
package maxdistructo.svrmgmt.server

import io.netty.channel.ChannelHandler.Sharable
import io.netty.channel.ChannelHandlerContext
import io.netty.channel.SimpleChannelInboundHandler
import io.netty.util.ReferenceCountUtil
import maxdistructo.svrmgmt.enums.CommunicationMessage
import maxdistructo.svrmgmt.enums.Side
import maxdistructo.svrmgmt.packets.*

import java.net.InetSocketAddress

/**
 * Handles a server-side channel.
 */
@Sharable
class TelnetServerHandler : SimpleChannelInboundHandler<String>() {

    private var isInShutdown = false

    @Throws(Exception::class)
    override fun channelActive(ctx: ChannelHandlerContext) {
        PacketLoginRequest.send(ctx, null, null)
    }

    override fun acceptInboundMessage(msg: Any?): Boolean {
        return true
    }

    @Throws(Exception::class)
    override fun channelRead(ctx: ChannelHandlerContext, request: Any?) {
        println("Recieved Message: $request")
        val splitMessage = request.toString().split(" ") //Messages are NetworkTag`Data`Args
        val remoteAddress = (ctx.channel().remoteAddress() as InetSocketAddress)
        val side = Side.SERVER
        when {
            splitMessage[0] == CommunicationMessage.LOGIN_SUCCESS.key -> {
                PacketLoginSuccess.respond(splitMessage, ctx, side)
            }
            splitMessage[0] == CommunicationMessage.LOGIN_FAIL.key -> {
                PacketLoginFailure.respond(splitMessage, ctx, side)
            }
            splitMessage[0] == CommunicationMessage.LOGIN.key -> {
                PacketLogin.respond(splitMessage, ctx, side)
            }
            splitMessage[0] == CommunicationMessage.ACKNOWLEDGE.key -> {
                PacketAcknowledge.respond(splitMessage, ctx, side)
            }
            splitMessage[0] == CommunicationMessage.ADD_PC.key -> {
                PacketAddPC.respond(splitMessage, ctx, side)
            }
            splitMessage[0] == CommunicationMessage.ONLINE_PC.key -> {
                PacketOnlinePC.respond(splitMessage, ctx, side)
            }
            splitMessage[0] == CommunicationMessage.DELETE_PC.key -> {
                PacketDeletePC.respond(splitMessage, ctx, side)
            }
            splitMessage[0] == CommunicationMessage.PC_UPDATE.key ->{
                PacketPCUpdate.respond(splitMessage, ctx, side)
            }
            splitMessage[0] == CommunicationMessage.MEMORY_UPDATE.key ->{
                PacketPCMemory.respond(splitMessage, ctx, side)
            }
            splitMessage[0] == CommunicationMessage.STORAGE_UPDATE.key ->{
                PacketPCStorage.respond(splitMessage, ctx, side)
            }
            else -> ReferenceCountUtil.release(request)
        }
    }

    @Throws(Exception::class)
    public override fun channelRead0(ctx: ChannelHandlerContext, request: String) {
    }

    override fun channelReadComplete(ctx: ChannelHandlerContext) {
        ctx.flush()
    }

    override fun exceptionCaught(ctx: ChannelHandlerContext, cause: Throwable) {
        cause.printStackTrace()
        ctx.close()
    }
}

