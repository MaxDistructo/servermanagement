package maxdistructo.svrmgmt.server

import io.netty.channel.ChannelHandlerContext
import io.netty.channel.ChannelId
import maxdistructo.svrmgmt.Utils
import maxdistructo.svrmgmt.server.login.Password
import java.util.*


//All access to the login database will need to go through this object to protect logins
object Database {
    private val databaseFile = Utils.readJSONFromFile("/database.json")
    private val authorizedTokens = LinkedList<Pair<String, ChannelId>>()

    fun checkLogin(username: String, password: String): Boolean{
        println("[DEBUG] [Database] [checkLogin] $username, $password")
        try {
            val storedPassword = databaseFile.getString(username)
            val hashedInput = Password.getSaltedHash(password)
            println("[DEBUG] [Database] [checkLogin] [try] Checking password against database. Stored+NewHash: $storedPassword, $password")
            return Password.check(password, storedPassword)
        }
        catch(e: Exception){
            e.printStackTrace()
            return false
        }
    }

    fun getToken(username: String, ctx: ChannelHandlerContext) : String{
        //val token = String(SecureRandom.getInstance("SHA1PRNG").generateSeed(32), Charset.forName("UTF-8"))
        val token = "TestTokensAreTheBest"
        authorizedTokens.add(Pair(token, ctx.channel().id()))
        return token
    }

    fun verifyToken(token: String, ctx: ChannelHandlerContext): Boolean{
        return verifyToken(token, ctx.channel().id())
    }

    fun verifyToken(token: String, id: ChannelId): Boolean{
        println("[DEBUG] Verifying token: $token")
        val ret = authorizedTokens.contains(Pair(token, id))
        println(ret)
        return ret
    }

    fun addUser(username: String, password: String){
        databaseFile.put(username, Password.getSaltedHash(password))
    }


}