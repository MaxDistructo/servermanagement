package maxdistructo.svrmgmt.server

import maxdistructo.svrmgmt.enums.UpdateOption
import org.json.JSONObject
import java.util.*

object PCUpdater {
    val updateQueue = mutableListOf<Triple<String, String, UpdateOption>>()
    val currentPCs = LinkedList<Pair<String,JSONObject>>()
    
    fun queueUpdate(pcName: String, data: String, action: UpdateOption){
        updateQueue.add(Triple(pcName,data,action))
    }
    
    fun run(){
        val pcName = updateQueue[0].first
        when (updateQueue[0].third) {
            UpdateOption.ADD_PC -> {
                addPC(pcName)
            }
            UpdateOption.DELETE_PC -> {
                deletePC(pcName)
            }
            UpdateOption.UPDATE_PC -> {
                val splitMessage = updateQueue[0].second.split("`")
                for(arg in splitMessage){
                    val split2 = arg.split("|")
                    updatePC(pcName, split2[0], split2[1])
                }
            }
            else -> {
                println("Invalid Update Option")
            }
        }

    }

    private fun addPC(pcName: String){
        currentPCs.add(Pair(pcName, JSONObject()))
    }

    private fun updatePCList(pcName: String, data: JSONObject){
        deletePC(pcName)
        currentPCs.add(Pair(pcName, data))
    }

    private fun deletePC(pcName : String){
        currentPCs.remove(getPC(pcName))
    }

    private fun getPC(pcName: String): Pair<String, JSONObject>{
        var obtained = Pair("", JSONObject())
        for(pc in currentPCs){
            if(pc.first == pcName){
                obtained = pc
                break
            }
        }
        return obtained
    }

    private fun updatePC(pcName: String, key: String, value: String){
        val pc = getPC(pcName)
        val data = pc.second
        data.remove(key)
        data.put(key, value)
        updatePCList(pc.first, data)
    }
}