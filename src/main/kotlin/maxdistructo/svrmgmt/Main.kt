package maxdistructo.svrmgmt

import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import maxdistructo.svrmgmt.server.Database
import maxdistructo.svrmgmt.server.PCUpdater
import maxdistructo.svrmgmt.server.TelnetServer
import maxdistructo.svrmgmt.serverclient.SrvTelnetClient
import maxdistructo.svrmgmt.wsclient.WSTelnetClient
import java.util.*

object Main {
    @JvmStatic
    fun main(args: Array<String>) {
        if (args.isEmpty()) {
            printHelp()
        } else if (args.size == 1) {
            when (args[0]) {
                "server" -> {
                    GlobalScope.launch {
                        TelnetServer.main(args)
                    }
                    while(true){
                        val input = Scanner(System.`in`)
                        val command = input.nextLine()
                        val splitCommand = command.split(" ")
                        when {
                            splitCommand[0] == "addUser" -> {
                                Database.addUser(splitCommand[1], splitCommand[2])
                            }
                            splitCommand[0] == "printDatabase" -> {
                                println(Utils.readJSONFromFile("/database.json"))
                            }
                            splitCommand[0] == "printPCs" -> {
                                println(PCUpdater.currentPCs)
                            }
                            splitCommand[0] == "printQueue" -> {
                                println(PCUpdater.updateQueue)
                            }
                        }
                    }
                }
                "client" -> SrvTelnetClient.main(args)
                "ws_client" -> WSTelnetClient.main(args)
                else -> printHelp()
            }
        }
    }
    fun printHelp(){
        println("This is a placeholder help statement")
    }
}