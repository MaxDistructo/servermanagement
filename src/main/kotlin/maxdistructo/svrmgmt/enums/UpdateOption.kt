package maxdistructo.svrmgmt.enums

enum class UpdateOption {
    ADD_PC,
    DELETE_PC,
    UPDATE_PC
}