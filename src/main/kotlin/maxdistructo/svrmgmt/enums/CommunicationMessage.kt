package maxdistructo.svrmgmt.enums

enum class CommunicationMessage(val key: String) {
    ACKNOWLEDGE("acknowledge"),
    END_PROGRAM("end"),

    LOGIN("login"),
    LOGIN_FAIL("login_fail"),
    LOGIN_SUCCESS("login_success"),
    LOGIN_REQUEST("login_request"),


    //Below are used with a secondary key of key_PCNAME
    STORAGE_UPDATE("updateStorage"),
    MEMORY_UPDATE("updateMemory"),
    CPU_UPDATE("updateCPU"),

    ADD_PC("addPC"), //Args of all important information about the system.
    DELETE_PC("deletePC"),
    ONLINE_PC("onlinePC"),
    PC_UPDATE("updatePC"), //All other updates not covered above that do not need to be done in real time. (Ex. Name, OS Version, etc.)

    //Args of key_PCNAME consoleName
    ADD_CONSOLE("addConsole"),
    DELETE_CONSOLE("deleteConsole"),
    CONSOLE_UPDATE("updateConsole")

}