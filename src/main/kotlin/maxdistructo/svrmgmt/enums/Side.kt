package maxdistructo.svrmgmt.enums

enum class Side {
    SERVER,
    WS_CLIENT,
    SERVER_CLIENT,
    CLIENT
}