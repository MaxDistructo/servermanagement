package maxdistructo.svrmgmt.packets

import io.netty.channel.ChannelHandlerContext
import maxdistructo.svrmgmt.enums.CommunicationMessage
import maxdistructo.svrmgmt.enums.Side

abstract class IPacket(val data: String) { //Yell at me all you want for this name. It's so I know this needs to be implemented and is abstract
    abstract val packetType : CommunicationMessage
    abstract fun respond(packetInfo: List<String>, ctx: ChannelHandlerContext, side: Enum<Side>)
    abstract fun send(ctx: ChannelHandlerContext, token: String?, optional: Any?)
}