package maxdistructo.svrmgmt.packets

import io.netty.channel.ChannelHandlerContext
import maxdistructo.svrmgmt.enums.CommunicationMessage
import maxdistructo.svrmgmt.enums.Side

class PacketAcknowledge(data: String) : IPacket(data) {
    override val packetType: CommunicationMessage
        get() = CommunicationMessage.ACKNOWLEDGE

    override fun respond(packetInfo: List<String>, ctx: ChannelHandlerContext, side: Enum<Side>) {
        println("Received Acknowledge. Packet: $packetInfo")
    }

    override fun send(ctx: ChannelHandlerContext, token: String?, optional: Any?) {
        ctx.writeAndFlush(packetType.key)
    }
}