package maxdistructo.svrmgmt.packets

import io.netty.channel.ChannelHandlerContext
import maxdistructo.svrmgmt.enums.CommunicationMessage
import maxdistructo.svrmgmt.enums.Side
import kotlin.system.exitProcess

class PacketEnd : IPacket() {
    override val packetType: CommunicationMessage
        get() = CommunicationMessage.END_PROGRAM

    override fun respond(packetInfo: List<String>, ctx: ChannelHandlerContext, side: Enum<Side>) {
        if(side == Side.WS_CLIENT){
            exitProcess(0)
        }
        if(side == Side.SERVER_CLIENT){
            exitProcess(0)
        }
    }

    override fun send(ctx: ChannelHandlerContext, token: String?, optional: Any?) {
        ctx.writeAndFlush(packetType.key)
    }
}