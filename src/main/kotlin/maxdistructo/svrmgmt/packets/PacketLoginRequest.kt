package maxdistructo.svrmgmt.packets

import io.netty.channel.ChannelHandlerContext
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import maxdistructo.svrmgmt.enums.CommunicationMessage
import maxdistructo.svrmgmt.enums.Side
import maxdistructo.svrmgmt.serverclient.SrvTelnetClient
import java.util.*

class PacketLoginRequest : IPacket() { //The start of all packet communication. Everything else is based on time from these communications.
    override val packetType: CommunicationMessage
        get() = CommunicationMessage.LOGIN_REQUEST

    override fun respond(packetInfo: List<String>, ctx: ChannelHandlerContext, side: Enum<Side>) {
        if(side == Side.SERVER_CLIENT){
            GlobalScope.launch {
                var credentials: Pair<String, String> = Pair("", "")
                while (SrvTelnetClient.TOKEN == "") {
                    credentials = login()
                    PacketLogin.send(ctx, credentials.first, credentials.second)
                    if (SrvTelnetClient.TOKEN == "") {
                        println("Incorrect Login. Please try again.")
                    }
                }
                while(true){} //To keep the connection open, infinite loop
            }
        }
    }

    override fun send(ctx: ChannelHandlerContext, token: String?, optional: Any?) {
        ctx.writeAndFlush(packetType.key)
    }

    private fun login(): Pair<String, String> {
        val scanner = Scanner(System.`in`)
        println("Please enter your user name: ")
        val username = scanner.nextLine()
        println("Please enter the password to ${username}@${SrvTelnetClient.HOST}")
        val password = scanner.nextLine()
        return Pair(username, password)
    }
}