package maxdistructo.svrmgmt.packets

import io.netty.channel.ChannelHandlerContext
import maxdistructo.svrmgmt.enums.CommunicationMessage
import maxdistructo.svrmgmt.enums.Side
import maxdistructo.svrmgmt.server.Database

class PacketLogin : IPacket() {
    override val packetType: CommunicationMessage
        get() = CommunicationMessage.LOGIN

    override fun respond(packetInfo: List<String>, ctx: ChannelHandlerContext, side: Enum<Side>) {
        var loginSuccess = false
        if(side == Side.SERVER) {
            loginSuccess = Database.checkLogin(packetInfo[1], packetInfo[2])
            if (loginSuccess) {
                PacketLoginSuccess.sendToken(ctx, packetInfo[1])
            } else {
                PacketLoginFailure.send(ctx, null, null)
            }
        }
        else{ //Report back to sender that we are not the server.
            ctx.writeAndFlush("debug I am not a Server!")
        }
    }

    override fun send(ctx: ChannelHandlerContext, token: String?, optional: Any?) {
        val username : String = token!!
        val password : String = optional!! as String
        ctx.writeAndFlush("$packetType.key $username $password")
    }
}