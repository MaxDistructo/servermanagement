package maxdistructo.svrmgmt.packets

import io.netty.channel.ChannelHandlerContext
import maxdistructo.svrmgmt.enums.CommunicationMessage
import maxdistructo.svrmgmt.enums.Side
import maxdistructo.svrmgmt.enums.UpdateOption
import maxdistructo.svrmgmt.server.Database
import maxdistructo.svrmgmt.server.PCUpdater
import maxdistructo.svrmgmt.serverclient.SrvTelnetClient
import oshi.SystemInfo

class PacketPCUpdate : IPacket() {
    override val packetType: CommunicationMessage
        get() = CommunicationMessage.PC_UPDATE

    override fun respond(packetInfo: List<String>, ctx: ChannelHandlerContext, side: Enum<Side>) {
        if(side == Side.SERVER_CLIENT){
            this.send(ctx, SrvTelnetClient.TOKEN, SrvTelnetClient.HOST)
        }
        else if(side == Side.WS_CLIENT){
            println("Update request recieved. Not responding as we are a access portal and not a client.")
        }
        if(side == Side.SERVER){
            if(Database.verifyToken(packetInfo[1], ctx.channel().id())){
                PCUpdater.queueUpdate(packetInfo[2], packetInfo[3], UpdateOption.UPDATE_PC)
            }
        }
    }

    override fun send(ctx: ChannelHandlerContext, token: String?, optional: Any?) {
        val si = SystemInfo()
        val os = si.operatingSystem
        val cpu = si.hardware.processor
        val memory = si.hardware.memory.physicalMemory
        val disks = si.hardware.diskStores
        val filestores = si.operatingSystem.fileSystem.getFileStores(true)
        val memBuilder = StringBuilder()
        val diskBuilder = StringBuilder()
        val filestoreBuilder = StringBuilder()
        var i = 0

        PacketPCStorage.send(ctx, null, null)
        PacketPCMemory.send(ctx, null, null)
        //PacketPCCPU.send(ctx)

        for(ram in memory){
            memBuilder.append("mem${i}Manufacturer|${ram.manufacturer}`mem${i}Size|${ram.capacity}`")
            i++
        }
        i = 0
        for(disk in disks){
            diskBuilder.append("disk${i}Manufacturer|${disk.model}`disk${i}Serial|${disk.serial} disk${i}Size|${disk.size}`")
            i++
        }
        i = 0 //Filestore variant to give used space per partition.
        for(part in filestores){
            filestoreBuilder.append("fs${i}Name|${part.name}`fs${i}Size|${part.totalSpace}`")
            i++
        }


        ctx.write("" +
                "$packetType " +
                "cpuName|${cpu.processorIdentifier}`cpuFrequency|${cpu.maxFreq}`cpuCores|${cpu.physicalProcessorCount}`cpuLogicalCores|${cpu.logicalProcessorCount}`" +
                "memoryCount|${memory.size}`memoryType|${memory[0].memoryType}`$memBuilder`" +
                "diskCount|${disks.size}`$diskBuilder`" +
                "osVersion|${os.versionInfo}`" +
                filestoreBuilder
        )
        ctx.flush()

    }
}