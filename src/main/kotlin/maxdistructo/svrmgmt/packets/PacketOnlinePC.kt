package maxdistructo.svrmgmt.packets

import io.netty.channel.ChannelHandlerContext
import maxdistructo.svrmgmt.enums.CommunicationMessage
import maxdistructo.svrmgmt.enums.Side
import maxdistructo.svrmgmt.enums.UpdateOption
import maxdistructo.svrmgmt.server.Database
import maxdistructo.svrmgmt.server.PCUpdater
import maxdistructo.svrmgmt.serverclient.SrvTelnetClient
import maxdistructo.svrmgmt.wsclient.WSTelnetClient
import java.time.Instant

class PacketOnlinePC : IPacket() {
    override val packetType: CommunicationMessage
        get() = CommunicationMessage.ONLINE_PC

    override fun respond(packetInfo: List<String>, ctx: ChannelHandlerContext, side: Enum<Side>) {
        if(side == Side.SERVER){
            if(Database.verifyToken(packetInfo[1], ctx.channel().id())) {
                PCUpdater.queueUpdate(packetInfo[2], "lastResponse|${Instant.now().toEpochMilli()}", UpdateOption.UPDATE_PC)
            }
        }
        else if(side == Side.WS_CLIENT){
            send(ctx, WSTelnetClient.TOKEN, WSTelnetClient.PC_NAME)
        }
        else if(side == Side.SERVER_CLIENT){
            send(ctx, SrvTelnetClient.TOKEN, SrvTelnetClient.PC_NAME)
        }
    }

    override fun send(ctx: ChannelHandlerContext, token: String?, optional: Any?) {
        ctx.writeAndFlush("${packetType.key} ${token!!} ${optional!!}")
    }
}