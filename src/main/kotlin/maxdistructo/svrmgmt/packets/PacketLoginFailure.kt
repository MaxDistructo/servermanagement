package maxdistructo.svrmgmt.packets

import io.netty.channel.ChannelHandlerContext
import maxdistructo.svrmgmt.enums.CommunicationMessage
import maxdistructo.svrmgmt.enums.Side

class PacketLoginFailure: IPacket() {
    override val packetType: CommunicationMessage
        get() = CommunicationMessage.LOGIN_FAIL

    override fun respond(packetInfo: List<String>, ctx: ChannelHandlerContext, side: Enum<Side>) {
        if(side == Side.CLIENT){
            TODO("Handle a relogin with the client")
        }
        else{ //Report back to caller for logging purposes
            ctx.writeAndFlush("debug I am not a Client!")
        }
    }

    override fun send(ctx: ChannelHandlerContext, token: String?, optional: Any?) {
        ctx.writeAndFlush(packetType.key)
    }
}