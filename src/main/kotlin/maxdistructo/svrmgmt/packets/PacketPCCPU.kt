package maxdistructo.svrmgmt.packets

import io.netty.channel.ChannelHandlerContext
import maxdistructo.svrmgmt.enums.CommunicationMessage
import maxdistructo.svrmgmt.enums.Side
import oshi.SystemInfo

class PacketPCCPU : IPacket(){
    override val packetType: CommunicationMessage
        get() = CommunicationMessage.CPU_UPDATE

    override fun respond(packetInfo: List<String>, ctx: ChannelHandlerContext, side: Enum<Side>) {
        if(side == Side.CLIENT){
            this.send(ctx, null, null)
        }
    }

    override fun send(ctx: ChannelHandlerContext, token: String?, optional: Any?) {
        val si = SystemInfo()
        val cpu = si.hardware.processor
        ctx.writeAndFlush("${packetType.key} cpuLoad|${cpu.systemCpuLoadTicks.sum()} ")
    }
}