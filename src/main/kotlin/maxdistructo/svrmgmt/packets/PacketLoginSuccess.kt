package maxdistructo.svrmgmt.packets

import io.netty.channel.ChannelHandlerContext
import maxdistructo.svrmgmt.enums.CommunicationMessage
import maxdistructo.svrmgmt.enums.Side
import maxdistructo.svrmgmt.server.Database
import maxdistructo.svrmgmt.serverclient.SrvTelnetClient

class PacketLoginSuccess: IPacket() {
    private var username = ""
    override val packetType: CommunicationMessage
        get() = CommunicationMessage.LOGIN_SUCCESS

    override fun respond(packetInfo: List<String>, ctx: ChannelHandlerContext, side: Enum<Side>) {
        if(side == Side.SERVER_CLIENT) {
            SrvTelnetClient.TOKEN = packetInfo[1] //We have received the token, apply it to the Client class
            PacketPCUpdate.send(ctx, SrvTelnetClient.TOKEN, SrvTelnetClient.PC_NAME)
        }
    }

    override fun send(ctx: ChannelHandlerContext, token: String?, optional: Any?) {
        val generatedToken: String = Database.getToken(username, ctx)
        ctx.writeAndFlush("${packetType.key} $generatedToken")
        username = ""
    }

    fun sendToken(ctx: ChannelHandlerContext, username: String){
        this.username = username
        this.send(ctx, null, null)
    }
}