package maxdistructo.svrmgmt.packets

import io.netty.channel.ChannelHandlerContext
import maxdistructo.svrmgmt.enums.CommunicationMessage
import maxdistructo.svrmgmt.enums.Side
import maxdistructo.svrmgmt.enums.UpdateOption
import maxdistructo.svrmgmt.server.Database
import maxdistructo.svrmgmt.server.PCUpdater
import oshi.SystemInfo

class PacketPCMemory : IPacket() {
    override val packetType: CommunicationMessage
        get() = CommunicationMessage.MEMORY_UPDATE

    override fun respond(packetInfo: List<String>, ctx: ChannelHandlerContext, side: Enum<Side>) {
        if(side == Side.CLIENT){
            this.send(ctx, null, null)
        }
        if(side == Side.SERVER){
            if(Database.verifyToken(packetInfo[1], ctx.channel().id())) {
                PCUpdater.queueUpdate(packetInfo[2], packetInfo[3], UpdateOption.UPDATE_PC)
            }
        }
    }

    override fun send(ctx: ChannelHandlerContext, token: String?, optional: Any?) {
        val si = SystemInfo()
        ctx.writeAndFlush("${packetType.key} pcMemUsage|${si.hardware.memory.available}")
    }
}