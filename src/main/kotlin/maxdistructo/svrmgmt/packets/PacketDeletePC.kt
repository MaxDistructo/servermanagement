package maxdistructo.svrmgmt.packets

import io.netty.channel.ChannelHandlerContext
import maxdistructo.svrmgmt.enums.CommunicationMessage
import maxdistructo.svrmgmt.enums.Side
import maxdistructo.svrmgmt.enums.UpdateOption
import maxdistructo.svrmgmt.server.Database
import maxdistructo.svrmgmt.server.PCUpdater

class PacketDeletePC : IPacket() {
    override val packetType: CommunicationMessage
        get() = CommunicationMessage.DELETE_PC

    override fun respond(packetInfo: List<String>, ctx: ChannelHandlerContext, side: Enum<Side>) {
        if(side == Side.SERVER){
            if(Database.verifyToken(packetInfo[1], ctx.channel().id())){
                PCUpdater.queueUpdate(packetInfo[2], "", UpdateOption.DELETE_PC)
            }
        }
    }

    override fun send(ctx: ChannelHandlerContext, token: String?, optional: Any?) {
        ctx.writeAndFlush("${packetType.key} ${token!!} ${optional!!}")
    }
}